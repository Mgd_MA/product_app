<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\User;
class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('products');
        Schema::create('products', function (Blueprint $table) {
            $table->id('product_id')->autoIncrement()->unique();
            $table->foreignId('User_id')->references('User_id')->on('users')->onDelete("cascade");
            $table->foreignId('category_id')->references('cate_id')->on('categories');
            $table->string('product_name');
            $table->string('product_image')->nullable();
            $table->integer('product_quantity')->default('0');
            $table->date('product_expire_date');
            $table->double('product_price');
            $table->text('product_desc');
            $table->string('Phone_number');
            $table->integer('product_likes')->default(0);
            $table->integer('product_dis_likes')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
