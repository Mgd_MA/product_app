<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discounts extends Model
{
    protected $fillable= [
        'date',
        'discount_percentage',
    ];
    public $timestamps=false;
    use HasFactory;
}
