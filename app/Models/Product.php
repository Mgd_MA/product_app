<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;



    protected $fillable= [
        'User_id',
        'category_id',
        'product_name',
        'product_image',
        'product_quantity',
        'product_expire_date',
        'product_price',
        'product_desc',
        "created_at",
        "updated_at",
        "Phone_number",
 ];
    protected $table="products";
    public $timestamps=true;
    protected $primaryKey="product_id";

    public function discounts(){
        return $this->hasMany(Discounts::class, 'product_id')->orderBy('date');
    }
}
