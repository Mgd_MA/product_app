<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Http\Resources\Json;


class CheckUserMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param $id
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $id=$request->route('id');
        $ut = DB::table('users')
            ->where('Email', '=', $id)
            ->first();
        if($ut==null){
            return response()->json(['message'=>'Not Found'],400);
        }
        $check_user = DB::table('users')
            ->where('Email', '=', $id)
            ->get();

        $check_userjs=json_decode($check_user,true);

        $token = $request->header('X-USER-TOKEN');

        $jsonStr = base64_decode($token);

        $jsonPayload = json_decode($jsonStr, true);
        if($check_userjs['0']['Email']!=$jsonPayload['email']){
            return response()->json(['message'=>'Not Allowed'],400);
        }
        return $next($request);
    }
}
