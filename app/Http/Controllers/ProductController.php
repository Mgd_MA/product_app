<?php

namespace App\Http\Controllers;
use App\Models\Product;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */


    public function index()
    {


       $input=DB::table('Products')->get();
        $input1=DB::table('Products')->first();
        if( $input1==null)
        {
            return response()->json(['message'=>'No products to display!!']);
        }

        return response()->json($input);
    }

public function Search(Request $request)
{
    $name=$request->input('name');
    $date=$request->input('EX_date');

        if(!isset($date))
        $products=DB::table('products')->where('product_name','=',$name)->get();
         if(!isset($name))
             $products=DB::table('products')->where('product_expire_date','=',$date)->get();

        return response()->json($products);

}

    public function Filter ($request)
    {
        $category_id=$request->query('category_id');
        $price_from=$request->query('price_from');
        $price_to=$request->query('price_to');
        $ProductQuery=Product::query();
        if($category_id) {
            $ProductQuery->where('category_id', $category_id);

        }
        if($price_from) {
            $ProductQuery->where('price','>=', $price_from);

        }

        if($price_to) {
            $ProductQuery->where('price','<', $price_to);

        }


        $product=$ProductQuery->get();
        return response()->json($product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {

        $token = $request->header('X-USER-TOKEN');


        $jsonStr = base64_decode($token);
        $jsonPayload = json_decode($jsonStr, true);
        $User=DB::table('users')->select('User_id')->where('Email','=',$jsonPayload['email'])->get();
        $User_id=json_decode($User,true);


        $rules = [
            'product_name' => 'required|string|min:2|max:255',
            'product_image' => 'max:10000',
            'product_quantity'=> 'required|integer|min:1',
            'product_expire_date'=> 'required|date_format:Y/m/d|after:tomorrow',
            'product_price'=> 'required|min:1',
            'product_desc' => 'min:6|required|string',
        ];
        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()->toArray()
            ]);
        }

        $product=Product::query()->create([

            'User_id'=>$User_id['0']['User_id'],
            'product_name' => $request->input('product_name'),
            'product_image' => $request->input('product_image'),
            'product_quantity'=> $request->input('product_quantity'),
            'product_expire_date'=>  $request->input('product_expire_date'),
            'product_price'=>  $request->input('product_price'),
            'product_desc' =>  $request->input('product_desc'),
            'category_id' => $request->input('category_id'),
            'Phone_number'=>$request->input('Phone_number'),
            "updated_at" => date('d/m/Y'),
            "created_at" => date('d/m/Y'),

        ]);
        foreach ($request->list_discounts as $discount){
            $product->discounts()->create([
                'date' => $discount['date'],
                'discount_percentage' => $discount['discount_percentage'],
            ]);
        }


        return response()->json(['message'=>'Data has been created successfully!']);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        DB::table('products')->insert($data);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $product=Product::find($id);

        if( $product==null)
    {
        return response()->json(['message'=>'The Requested Product Is Not Available!!']);
    }

        $now=now();
        $now1=Carbon::parse($now);
        $discounts = $product->discounts()->get();
        $maxDiscount = null;
        foreach ($discounts as $discount){
            if (Carbon::parse($discount['date']) <= $now1){
                $maxDiscount = $discount;
            }
        }
        if (!is_null($maxDiscount)){
            $discount_value =
                ($product->product_price*$maxDiscount['discount_percentage'])/100;
          $product['product_price'] = $product->product_price - $discount_value;
        }
        $user=DB::table('users')->select('Phone_number')->where('User_id','=',$product['User_id'])->get();
        $user_phone=json_decode($user,true);

        $data=[
            'product_id'=>$product['product_id'],
            'User_id'=>$product['User_id'],
            'category_id'=>$product['category_id'],
            'product_name'=>$product['product_name'],
            'Phone_number'=>$product['Phone_number'],
            'product_image'=>$product['product_image'],
            'product_quantity'=>$product['product_quantity'],
            'product_expire_date'=>$product['product_expire_date'],
            'product_price'=>$product['product_price'],
            'product_desc'=>$product['product_desc'],

        ];
        return response()->json($data);


    }
    public function ShowMyProduct(Request $request)
    {
        $token = $request->header('X-USER-TOKEN');


        $jsonStr = base64_decode($token);
        $jsonPayload = json_decode($jsonStr, true);
        $User=DB::table('users')->select('User_id')->where('Email','=',$jsonPayload['email'])->get();
        $User_id=json_decode($User,true);
        $product=DB::table('products')->where('User_id','=',$User_id['0']['User_id'])->get();
        return response()->json($product);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function edit(Request $request,$id)
    {

        $input=$request->all();
        $rules = [
        'product_name' => 'string|min:2|max:255',
        'product_image' => 'max:10000',
        'product_quantity'=> 'integer|min:1',
        'product_price'=> 'min:1',
        'product_desc' => 'min:6|string',
    ];
        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()){
            return response()->json(['success' => false, 'errors' => $validator->errors()->toArray()]);
        }

        $kr=DB::table('products')->where('product_id','=', $id)->get();
        if( $kr->isEmpty())
        {
            return response()->json(['message'=>'This product is not available!!']);
        }
        DB::table('products')->where('product_id', $id)->update($input);

        return response()->json(['message'=>'Data has been updated successfully!']);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return JsonResponse
     */


    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {   $pru=null;
        $pru=DB::table('Products')->where('product_id','=',$id)->get();

        if( $pru->isEmpty())
        {
            return response()->json(['message'=>'Not Found']);
        }
       // return response()->json(['data','$pro']);
       DB::table('Products')->where('product_id',$id)->delete();
        return response()->json(['success' => true,'message'=>'product has been deleted successfully!']);    }
    }
