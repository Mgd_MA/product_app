<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{

    public function create(Request $request,$id)
    {
        $rules = [
            'comment'=> 'required|string',
        ];

        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()->toArray()
            ]);
        }

        $token = $request->header('X-USER-TOKEN');

        $jsonStr = base64_decode($token);

        $jsonPayload = json_decode($jsonStr, true);
         $check_user = DB::table('users')
        ->where('Email', '=', $jsonPayload['email'])
        ->get('User_id');
         $kr=json_decode($check_user,true);
         $user_name = DB::table('users')
             ->where('Email', '=', $jsonPayload['email'])
             ->get('User_name');
         $kr2=json_decode($user_name,true);
         $data=[
             'product_id'=>$id,
             'User_id'=>$kr['0']['User_id'],
             'Username'=>$kr2['0']['User_name'],
             'comment'=>$request->input('comment'),
         ];
        DB::table('comments')->insert($data);

    }

    public function show(Comment $comment,$id)
    {
        $pc = DB::table('comments')
            ->where('product_id', '=', $id)
            ->first();
        if($pc==null){
            return response()->json(['message'=>'there is no comment for this product'],404);
        }
        $ut = DB::table('comments')->select('Username','comment')
            ->where('product_id', '=', $id)
            ->get();

        return response()->json(['Comments'=>$ut]);
    }


}
