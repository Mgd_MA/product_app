<?php

namespace App\Http\Controllers;



use Illuminate\Support\Facades\DB;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */



    public function show($cate_name)
    {
        $cc = DB::table('categories')
        ->where('category_name', '=', $cate_name)
        ->first();
        if($cc==null){
            return response()->json(['message'=>'Category Not Found'],404);
        }

        $ut = DB::table('categories')
            ->where('category_name', '=', $cate_name)->get('cate_id');
        $uj=json_decode($ut,true);

        return response()->json($uj['0']['cate_id']);
    }



}
