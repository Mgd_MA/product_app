<?php

namespace App\Http\Controllers;

use App\Models\User;

use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\String_;
use function PHPUnit\Framework\throwException;
use Illuminate\Http\Resources\Json;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return \response()->json(DB::table('users')->select('User_name','Email','phone_number')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request)
    {

        $rules = [
            'Email' => 'required|string|email|max:255|unique:users,Email',
            'Phone_number'=> 'required|string|unique:users,Phone_number',
        ];
        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()->toArray()
            ]);
        }


           $data = $request->input();
           DB::table('users')->insert($data);
        $token=base64_encode(json_encode([
            "email" => $request->input('Email'),
            "password" => $request->input('password'),
            "logged_at" => date('d m Y'),
            "expired_at" => date('d m Y'),
            "user_role" => "user"]));


        return response()->json(['Token'=>$token]);

    }


    /**
     * Show the form for login.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $check_user=DB::table('users')->where('Email','=',$request->input('email'))->
        where('password','=',$request->input('password'))->first();
        if(!$check_user){
            return response()->json(['success'=>false,'message'=>'Email or password is invalid plz try again']);
        }
        $token=base64_encode(json_encode([
            "email" => $request->input('email'),
            "password" => $request->input('password'),
            "logged_at" => date('d m Y'),
            "expired_at" => date('d m Y'),
            "user_role" => "user"]));


                return response()->json(['Token'=>$token]);

    }

    public function logout(Request $request){
        $token=base64_encode(json_encode([""]));

        return response()->json(['Token'=>$token]);
    }


    /**
     * Display the specified resource.
     *
     *
     */
    public function show(Request $request)
    {
        $token = $request->header('X-USER-TOKEN');
        $jsonStr = base64_decode($token);
        $jsonPayload = json_decode($jsonStr, true);
        $User=DB::table('users')->select('User_name','Email','phone_number')->
        where('Email','=',$jsonPayload['email'])->get();
        $User_info=json_decode($User,true);

       return  response()->json($User_info);

    }



    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request,$id)
    {

        $u = DB::table('users')
            ->where('Email', '=', $id)
            ->get();
        $uj=json_decode($u,true);

        $rules = [
            'Phone_number'=> 'required|string',
        ];

        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()->toArray()
            ]);
        }
        else{
            $foi=$request->input('Phone_number');
            if($foi==$uj['0']['Phone_number'])
                $data=[
                    'User_name'=>$request->input('User_name')
                ];
            else
            $data=$request->input();

            DB::table('users')
                ->where('User_id', $uj['0']['User_id'])
                ->update($data);
            return response()->json(['message'=>'Done !'],200);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return JsonResponse
     */
    public function destroy(Request $request,$id)
    {
        $u = DB::table('users')
            ->where('Email', '=', $id)
            ->get();
        $uj=json_decode($u,true);

        DB::table('users')
            ->where('User_id', $uj['0']['User_id'])
            ->delete();
        return response()->json(['message'=>'Done !'],200);

    }
}
