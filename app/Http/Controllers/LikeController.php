<?php

namespace App\Http\Controllers;

use App\Models\Like;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LikeController extends Controller
{

    public function create(Request $request,$id)
    {
        $rules = [
            'dis/like'=> 'required|integer|min:-1|max:1',
        ];

        $validator = Validator::make(request()->all(), $rules);
        if ($validator->fails()){
            return response()->json([
                'success' => false,
                'errors' => $validator->errors()->toArray()
            ]);
        }

        $token = $request->header('X-USER-TOKEN');
        $jsonStr = base64_decode($token);
        $jsonPayload = json_decode($jsonStr, true);
        $check_user = DB::table('users')
            ->where('Email', '=', $jsonPayload['email'])
            ->get('User_id');
        $kr=json_decode($check_user,true);

        $ut  =DB::table('likes')
            ->where('User_id', $kr['0']['User_id'])
            ->where('product_id','=',$id)
            ->first();

        if($ut==null){

            $data=[
                'product_id'=>$id,
                'User_id'=>$kr['0']['User_id'],
                'Dis/like'=>$request->input('dis/like'),
            ];

            DB::table('likes')->insert($data);
        }
        else{
            $data=[
                'Dis/like'=>$request->input('dis/like'),
            ];

            DB::table('likes')
                ->where('User_id', $kr['0']['User_id'])
                ->where('product_id','=',$id)
                ->update($data);

        }

    }

    public function show($id)
    {

        $lc = DB::table('likes')
            ->where('product_id', '=', $id)
            ->first();

        if($lc==null){
            return response()->json(['message'=>'there is no likes or dislikes for this product'],404);
        }
        $likes = DB::table('likes')
            ->where('product_id', '=', $id)
            ->where('Dis/like','=',1)
            ->count();
        $dislikes = DB::table('likes')
            ->where('product_id', '=', $id)
            ->where('Dis/like','=',-1)
            ->count();
        return response()->json(['likes'=>$likes,'DisLikes'=>$dislikes]);
    }


}
