<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\ProductController;
use App\Http\Middleware\CheckTokenMiddleware ;
use App\Http\Middleware\CheckUserMiddleWare;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('user/', [UserController::class, 'index']);
Route::post('user/', [UserController::class, 'create']);
Route::post('user/login', [UserController::class, 'login']);

Route::middleware([CheckTokenMiddleware::class])->group(function () {


    Route::prefix('user')->group(function () {
        Route::post('/logout', [UserController::class, 'logout']);
        Route::get('/info',[UserController::class,'show']);
        Route::middleware([CheckUserMiddleWare::class])->group(function (){

            Route::put('/{id}',[UserController::class,'update']);
            Route::delete('/{id}',[UserController::class,'destroy']);
        });

    });

    Route::prefix('product')->group(function () {
        Route::get('/',[ProductController::class ,'index']);
        Route::post('/',[ProductController::class ,'create']);
        Route::get('/{id}',[ProductController::class ,'show']);
        Route::put('/{id}',[ProductController::class ,'edit']);
        Route::delete('/{id}',[ProductController::class ,'destroy']);

    });
    Route::get('/find',[ProductController::class,'Search']);

    Route::prefix('category')->group(function () {
        Route::post('/', [CategoryController::class, 'create']);
        Route::get('/{cate_name}', [CategoryController::class, 'show']);
    });


        Route::post('comment/{id}', [CommentController::class, 'create']);
        Route::get('comment/{id}', [CommentController::class, 'show']);

        Route::post('action/{id}', [LikeController::class, 'create']);
        Route::get('action/{id}', [LikeController::class, 'show']);


});

Route::get('myproduct', [ProductController::class, 'ShowMyProduct']);









